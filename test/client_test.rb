# frozen_string_literal: true
require 'minitest/autorun'
require './lib/mary'

describe Mary::Client do
  it 'connectes to tha server' do
    assert_includes Mary::Client.new.ping, 'MARY Web Client'
  end

  it 'connectes to tha server' do
    Mary::Client.new.voices.tap do |voices|
      assert_equal voices.size, 3
      assert_includes voices, Mary::Voice.new('dfki-spike en_GB male unitselection general')
      assert_includes voices, Mary::Voice.new('dfki-obadiah en_GB male unitselection general')
      assert_includes voices, Mary::Voice.new('dfki-obadiah en_GB male unitselection general')
    end
  end

  it 'converts phrase to audio' do
    Mary::Client.new.phrase_to_speech(text: 'We are in the simple testing phrase.').tap do |file|
      assert_kind_of Tempfile, file
      assert_operator file.size, :>, 0
    end
  end

  it 'converts long text to audio' do
    text = 'We are in the simple testing phrase.\nWe are in the simple testing phrase'

    Mary::Client.new.text_to_speech(text: text).tap do |file|
      assert_kind_of File, file
      assert_operator file.size, :>, 0
    end
  end
end
