# mary tts server api client in ruby
## usage

    require './lib/mary'
    text = "A rabbit’s brain has been successfully cryogenically preserved in long-term storage, marking the first time a whole mammalian brain has been kept in “near-perfect” condition during this process. It marks a significant breakthrough in the field of cryonics and boosts the prospect of one day bringing frozen human brains back to life. Researchers from 21st Century Medicine (21CM) used a new technique called Aldehyde-stabilized cryopreservation that filled the vascular system of the rabbit brain with chemicals that would allow it to be cooled to -211 degrees Fahrenheit (-135 degrees Celsius)."

    files = Mary::Client.new.voices.map(&:name).each do |voice|
      Mary::Client.new.text_to_speech(text: text, voice: voice)
    end

possible output:

    /tmp/article-1476486238-dfki-spike-hsmm.mp3
    /tmp/article-1476486242-dfki-spike.mp3
    /tmp/article-1476486255-dfki-prudence-hsmm.mp3
    /tmp/article-1476486261-dfki-prudence.mp3
    /tmp/article-1476486274-dfki-poppy-hsmm.mp3
    /tmp/article-1476486277-dfki-poppy.mp3
    /tmp/article-1476486302-dfki-obadiah-hsmm.mp3
    /tmp/article-1476486307-dfki-obadiah.mp3
    /tmp/article-1476486311-cmu-slt.mp3
    /tmp/article-1476486322-cmu-rms-hsmm.mp3
    /tmp/article-1476486326-cmu-rms.mp3
    /tmp/article-1476486331-cmu-bdl-hsmm.mp3
    /tmp/article-1476486335-cmu-bdl.mp3


## server & development

### build docker container

    docker build -t 'godot-mary' .

### run server

    docker run -d -p 59125:59125 godot-mary

### run script

    MARY_SERVER_URL=http://localhost:59125 ruby convert.rb
