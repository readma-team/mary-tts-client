# coding: utf-8
# frozen_string_literal: true
require './lib/mary'
text = "A rabbit's brain has been successfully cryogenically preserved in long-term storage, marking the first time a whole mammalian brain has been kept in 'near-perfect' condition during this process. It marks a significant breakthrough in the field of cryonics and boosts the prospect of one day bringing frozen human brains back to life. Researchers from 21st Century Medicine (21CM) used a new technique called Aldehyde-stabilized cryopreservation that filled the vascular system of the rabbit brain with chemicals that would allow it to be cooled to -211 degrees Fahrenheit (-135 degrees Celsius)."

Mary::Client.new.voices.map(&:name).map do |voice|
  puts Mary::Client.new.text_to_speech(text: text, voice: voice).path
end
