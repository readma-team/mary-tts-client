FROM anapsix/alpine-java:latest

MAINTAINER godot <godot@tlen.pl>

RUN apk update && apk add ca-certificates wget && update-ca-certificates && apk add openssl

RUN mkdir marytts \
    && wget -O installer.zip https://github.com/marytts/marytts-installer/archive/v5.2.zip \
    && unzip -d marytts installer.zip \
    && rm installer.zip \
    && cd /marytts/marytts-installer-5.2 \
    && ./marytts "install:dfki-obadiah-hsmm" \
    && ./marytts "install:cmu-rms-hsmm"

    # && ./marytts "install:cmu-rms" \
    # && ./marytts "install:cmu-slt" \
    # && ./marytts "install:dfki-obadiah" \
    # && ./marytts "install:dfki-spike" \
    # && ./marytts "install:dfki-spike-hsmm" \
    # && ./marytts "install:cmu-bdl" \
    # && ./marytts "install:cmu-bdl-hsmm" \
    # && ./marytts "install:dfki-poppy" \
    # && ./marytts "install:dfki-poppy-hsmm" \
    # && ./marytts "install:dfki-prudence" \
    # && ./marytts "install:dfki-prudence-hsmm"

CMD cd /marytts/marytts-installer-5.2 && ./marytts server

EXPOSE 59125