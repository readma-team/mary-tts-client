# frozen_string_literal: true
require 'faraday'
require 'faraday_middleware'
require 'faraday_middleware/response_middleware'

require_relative 'http_client'
require_relative 'response'
require_relative 'phrase_to_speech'
require_relative 'text_to_speech'

module Mary
  class Voice
    attr_reader :name, :raw

    def initialize(raw)
      @raw = raw.strip.split(' ')
    end

    def name
      @raw.first
    end

    def ==(other)
      @raw == other.raw
    end
  end

  class Client
    def voices
      connection
        .get('/voices')
        .lines
        .map { |line| Voice.new(line) }
    end

    def default_voice
      voices.first.name
    end

    def ping
      connection.get '/'
    end

    def text_to_speech(text:, voice: default_voice)
      Mary::TextToSpeech.new(text: text, voice: voice).execute
    end

    def phrase_to_speech(text:, voice: default_voice)
      Mary::PhraseToSpeech.new(text: text, voice: voice).execute
    end

    private

    def connection
      Mary::HttpClient.new
    end
  end
end
