# frozen_string_literal: true
module Mary
  class Response
    def initialize(response)
      @response = response
    end

    def body
      @response.body
    end

    def success?
      @response.status == 200
    end
  end
end
