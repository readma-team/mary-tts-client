# frozen_string_literal: true
module Mary
  class TextToSpeech
    def initialize(text:, voice:)
      @text = text
      @voice = voice
    end

    def execute
      convert_audio_files
      merge_audio_files
      output_file
    end

    private

    def merge_audio_files
      audio_files
        .map { |file| "#{file.path}.mp3" }
        .each(&method(:append_file))
    end

    def append_file(filename)
      `cat #{filename} >> #{output_filename}`
    end

    def convert_audio_files
      audio_files.each(&method(:convert))
    end

    def convert(file)
      `lame -h -b320 #{file.path} #{file.path}.mp3 2>/dev/null`
    end

    def audio_files
      @audio_files ||= phrases.map(&method(:phrase_to_speech))
    end

    def phrase_to_speech(phrase)
      Mary::PhraseToSpeech
        .new(text: phrase, voice: @voice)
        .execute
    end

    def phrases
      @text.split("\n")
    end

    def output_file
      File.new(output_filename)
    end

    def output_filename
      @output_filename ||= "/tmp/article-#{Time.now.to_i}-#{@voice}.mp3"
    end
  end
end
