# frozen_string_literal: true
require 'tempfile'

module FaradayMiddleware
  class ParseAudio < ResponseMiddleware
    define_parser do |body|
      file = Tempfile.new('audio', '/tmp')
      file.binmode
      file.write body
      file
    end
  end
end

Faraday::Response.register_middleware audio: FaradayMiddleware::ParseAudio
