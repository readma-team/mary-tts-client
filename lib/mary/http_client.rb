# frozen_string_literal: true
require_relative './parse_audio'
module Mary
  class HttpClient
    def get(path, params = {})
      handle_response connection.get(path, params)
    end

    def post(path, params = {})
      handle_response connection.post(path, params)
    end

    def handle_response(response)
      Response.new(response).tap do |r|
        raise 'There was a problem with the server. Please check logs.' unless r.success?
      end.body
    end

    private

    def connection
      Faraday.new(url: mary_server_url) do |faraday|
        faraday.request :url_encoded
        faraday.response :audio, content_type: /audio\/x-wav/
        faraday.response :logger if ENV['HTTP_LOG']
        faraday.adapter Faraday.default_adapter
      end
    end

    def mary_server_url
      ENV['MARY_SERVER_URL']
    end
  end
end
