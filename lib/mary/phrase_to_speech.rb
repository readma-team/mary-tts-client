# frozen_string_literal: true
module Mary
  class PhraseToSpeech
    def initialize(text:, voice: 'dfki-spike')
      @voice = voice
      @text  = text
    end

    def execute
      connection.post 'process',
                      'INPUT_TEXT' => @text,
                      'INPUT_TYPE' => 'TEXT',
                      'VOICE' => @voice,
                      'LOCALE' => 'en_US',
                      'OUTPUT_TYPE' => 'AUDIO',
                      'AUDIO' => 'WAVE_FILE'
    rescue
      puts 'PhraseToSpeech#failed'
      puts $ERROR_INFO.message
    end

    private

    def connection
      HttpClient.new
    end
  end
end
