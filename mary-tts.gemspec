Gem::Specification.new do |spec|
  spec.name        = 'mary-tts'
  spec.version     = '0.0.1'
  spec.date        = '2016-09-28'
  spec.summary     = 'Mary TTS Ruby Client'
  spec.description = 'Mary TTS Ruby Client'
  spec.authors     = ['Dariusz Gorzeba']
  spec.email       = 'godot@tlen.pl'
  spec.files       = ['lib/mary.rb']
  spec.license     = 'MIT'
  spec.add_dependency 'faraday'
  spec.add_dependency 'faraday_middleware'
end
